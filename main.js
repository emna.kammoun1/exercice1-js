class User {
    constructor(id,name,email) {
        this.id=id
        this.name=name
        this.email=email
    }
    toString() {
        return 'Le client qui a id : ' + this.id + " de nom : " + this.name + ' son email est : ' + this.email
    }

    // le domaine de l'adresse e-mail de l'utilisateur
    async getEmailDomain(){
        const rep = await fetch("https://my-json-server.typicode.com/typicode/demo/profile");
        const domaine = this.email.split("@")
        return  domaine[1]
    }

    // génère un identifiant unique pour un nouvel utilisateur
    static generateId() {          
        return Math.floor((1 + Math.random()) * 0x10000)
        .toString(16)
        .substring(1)
    }
}
//crreer les utilisateurs
const c1 = new User(User.generateId(),'Emna','emna.kammoun@enis.tn')
const c2 = new User(User.generateId(),'John','Johnkammoun297@gmail.com')
const c3 = new User(User.generateId(),'Jamel','Jamel.kammoun@gmail.tn')


const users=[]
users.push(c1)
users.push(c2)
users.push(c3)

// filtrer les utilisateurs dont le nom commence par "J"
const result = users.filter(user =>user.name[0]=='J')
console.log(' les utilisateurs dont le nom commence par "J" sont :' ,result)

// chercher les noms
const tab = users.map(user =>user.name)
console.log('les noms des utilisateurs : ' , tab)



console.log('***********')
users.forEach(user => console.log(user.toString()))
console.log('***********')


users.forEach(async user => {
    console.log('Le domaine adresse e-mail de ',user.email, ' est : ' ,await user.getEmailDomain())
  })